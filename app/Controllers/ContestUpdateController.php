<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\ContestRepositoryInterface;
use App\Model\ValueObject\ContestValueObject;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ContestUpdateController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestUpdateController extends DefaultController
{

    /**
     * @var ContestRepositoryInterface
     */
    private $contestRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ContestUpdateController constructor.
     * @param ContestRepositoryInterface $contestRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(
        ContestRepositoryInterface $contestRepository,
        EntityManagerInterface $em
    )
    {
        $this->contestRepository = $contestRepository;
        $this->em = $em;

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $data = $request->getParsedBody();
                $entity = $this->contestRepository->findContest(intval($args['id']));

                if (is_array($data) && count($data)) {
                    if (array_key_exists('active', $data)) {
                        $entity->setActive(boolval($data['active']));
                    }

                    if (array_key_exists('contestUrl', $data)) {
                        $entity->setContestUrl((string)($data['contestUrl'] ?? ''));
                    }

                    if (array_key_exists('order', $data)) {
                        $entity->setOrder(intval($data['order']));
                    }
                }

                $this->em->flush();

                return $response->withJson(new ContestValueObject($entity), 200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            }
        }

        return $response->withStatus(400);
    }
}
