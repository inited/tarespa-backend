<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 13. 03. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\NotificationRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class NotificationDeleteController
 * @package App\Controllers
 */
final class NotificationDeleteController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var NotificationRepositoryInterface
     */
    private $notificationRepository;

    /**
     * NotificationDeleteController constructor.
     *
     * @param EntityManagerInterface          $em
     * @param NotificationRepositoryInterface $notificationRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        NotificationRepositoryInterface $notificationRepository
    ) {
        $this->em = $em;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $entity = $this->notificationRepository->findNotification(intval($args['id']));

                $this->em->remove($entity);
                $this->em->flush();

                return $response->withStatus(200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            }
        }

        return $response->withStatus(400);
    }

}
