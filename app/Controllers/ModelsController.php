<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\Model;
use App\Model\Repository\ModelRepositoryInterface;
use App\Model\ValueObject\ModelValueObject;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ModelsController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ModelsController extends DefaultController
{

    /**
     * @var ModelRepositoryInterface
     */
    private $modelRepository;

    /**
     * ModelsController constructor.
     * @param ModelRepositoryInterface $modelRepository
     */
    public function __construct(ModelRepositoryInterface $modelRepository)
    {
        $this->modelRepository = $modelRepository;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function defaultAction(Request $request, Response $response)
    {
        try {
            $data = [];

            /** @var Model[] $models */
            $models = $this->modelRepository->findAll();

            foreach ($models as $model) {
                $data[] = new ModelValueObject($model);
            }

            return $response->withJson($data, 200);
        } catch (\Exception $e) {
            return $response->withStatus(500);
        }
    }
}
