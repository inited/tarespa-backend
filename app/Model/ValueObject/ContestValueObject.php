<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\ValueObject;

use App\Model\Entity\Contest;

/**
 * Class ContestValueObject
 * @package App\Model\ValueObject
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestValueObject
{

    /**
     * @var integer
     */
    public $id;

    /**
     * @var bool
     */
    public $active = true;

    /**
     * @var string
     */
    public $contestUrl = '';

    /**
     * @var int
     */
    public $order = 1;

    /**
     * @var integer
     */
    public $timestamp;

    /**
     * ContestValueObject constructor.
     * @param Contest $contest
     */
    public function __construct(Contest $contest)
    {
        $this->id = $contest->getId();
        $this->active = $contest->isActive();
        $this->contestUrl = $contest->getContestUrl();
        $this->order = $contest->getOrder();
        $this->timestamp = $contest->getTimestamp()->getTimestamp();
    }
}
