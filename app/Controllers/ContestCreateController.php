<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\Contest;
use App\Model\ValueObject\ContestValueObject;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ContestCreateController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestCreateController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ContestCreateController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (is_array($data) && count($data)) {
            $entity = new Contest();

            if (array_key_exists('active', $data)) {
                $entity->setActive(boolval($data['active']));
            }

            if (array_key_exists('contestUrl', $data)) {
                $entity->setContestUrl((string)($data['contestUrl'] ?? ''));
            }

            if (array_key_exists('order', $data)) {
                $entity->setOrder(intval($data['order']));
            }

            $this->em->persist($entity);
            $this->em->flush();

            return $response->withJson(new ContestValueObject($entity), 200);

        }

        return $response->withStatus(400);
    }

}
