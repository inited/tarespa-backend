<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\ContestRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ContestDeleteController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestDeleteController extends DefaultController
{

    /**
     * @var ContestRepositoryInterface
     */
    private $contestRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ContestDeleteController constructor.
     * @param ContestRepositoryInterface $contestRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(
        ContestRepositoryInterface $contestRepository,
        EntityManagerInterface $em
    )
    {
        $this->contestRepository = $contestRepository;
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $entity = $this->contestRepository->findContest(intval($args['id']));

                $this->em->remove($entity);
                $this->em->flush();

                return $response->withStatus(200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            }
        }

        return $response->withStatus(400);
    }

}
