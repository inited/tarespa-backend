<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\ValueObject;

use App\Model\Entity\Model;

/**
 * Class ModelValueObject
 * @package App\Model\ValueObject
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ModelValueObject
{

    /**
     * @var integer
     */
    public $id;

    /**
     * @var bool
     */
    public $active = true;

    /**
     * @var string
     */
    public $buyUrl = '';

    /**
     * @var string
     */
    public $category = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var UrlEmbeddableValueObject
     */
    public $model;

    /**
     * @var ModelGalleryItemValueObject[]
     */
    public $gallery = [];

    /**
     * @var int
     */
    public $order = 1;

    /**
     * @var UrlEmbeddableValueObject
     */
    public $picture;

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var int
     */
    public $updated;

    /**
     * @var string
     */
    public $viewUrl = '';

    /**
     * ModelValueObject constructor.
     * @param Model $model
     * @throws \Exception
     */
    public function __construct(Model $model)
    {
        $this->id = $model->getId();
        $this->active = $model->isActive();
        $this->buyUrl = $model->getBuyUrl();
        $this->category = $model->getCategory();
        $this->description = $model->getDescription();
        $this->model = new UrlEmbeddableValueObject($model->getModel());
        $this->order = $model->getOrder();
        $this->picture = new UrlEmbeddableValueObject($model->getPicture());
        $this->title = $model->getTitle();
        $this->updated = $model->getUpdated()->getTimestamp();
        $this->viewUrl = $model->getViewUrl();

        foreach ($model->getGallery() as $gallery) {
            $this->gallery[] = new ModelGalleryItemValueObject($gallery);
        }
    }
}
