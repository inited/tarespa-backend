<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Repository\ContestRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ContestsController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestsController extends DefaultController
{

    /**
     * @var ContestRepositoryInterface
     */
    private $contestRepository;

    /**
     * ContestsController constructor.
     * @param ContestRepositoryInterface $contestRepository
     */
    public function __construct(ContestRepositoryInterface $contestRepository)
    {
        $this->contestRepository = $contestRepository;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function defaultAction(Request $request, Response $response)
    {
        return $response->withJson($this->contestRepository->findContests(), 200);
    }
}
