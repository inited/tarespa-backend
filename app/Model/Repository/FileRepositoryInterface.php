<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Interface FileRepositoryInterface
 * @package App\Model\Repository
 */
interface FileRepositoryInterface extends BaseRepositoryInterface
{

}
