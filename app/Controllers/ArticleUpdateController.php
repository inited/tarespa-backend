<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\ArticleRepositoryInterface;
use App\Model\ValueObject\ArticleValueObject;
use Doctrine\DBAL\Exception\ConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ArticleUpdateController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArticleUpdateController extends DefaultController
{

    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ArticleUpdateController constructor.
     * @param EntityManagerInterface $em
     * @param ArticleRepositoryInterface $articleRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        ArticleRepositoryInterface $articleRepository
    )
    {
        $this->articleRepository = $articleRepository;
        $this->em = $em;

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $data = $request->getParsedBody();
                $entity = $this->articleRepository->findArticle(intval($args['id']));

                if (is_array($data) && count($data)) {
                    if (array_key_exists('key', $data)) {
                        $entity->setKey((string)($data['key'] ?? ''));
                    }

                    if (array_key_exists('text', $data)) {
                        $entity->setText((string)($data['text'] ?? ''));
                    }
                }

                $this->em->flush();

                return $response->withJson(new ArticleValueObject($entity), 200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            } catch (ConstraintViolationException $e) {
                $data = [
                    'error' => '"Key" is not unique!'
                ];

                return $response->withJson($data, 400);
            }
        }

        return $response->withStatus(400);
    }
}
