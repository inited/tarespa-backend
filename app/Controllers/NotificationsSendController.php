<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\Notification;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class NotificationsSendController
 * @package App\Controllers
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
final class NotificationsSendController extends DefaultController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * NotificationsSendController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $requestData = $request->getParsedBody();

        if (is_array($requestData) && count($requestData)) {
            $notification = new Notification();
            $notification->setDescription($requestData['description'] ?? '');
            $notification->setPage($requestData['page'] ?? '');
            $notification->setTitle($requestData['title'] ?? '');

            $this->em->persist($notification);

            $topic = $requestData['test'] ? '/topics/test' : '/topics/news';
            $notificationJson = [
                "to" => $topic,
                "notification" => [
                    "body" => $requestData['description'],
                    "title" => $requestData['title'],
                    "icon" => "ic_notification",
                    "click_action" => "FCM_PLUGIN_ACTIVITY",
                ],
                "data" => [
                    "body" => $requestData['description'],
                    "title" => $requestData['title'],
                    "page" => $requestData['page'],
                ],
            ];

            $notificationData = json_encode($notificationJson, JSON_FORCE_OBJECT);
            //FCM API end-point
            $url = 'https://fcm.googleapis.com/fcm/send';
            //api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
            $server_key = 'AAAAXwDXTmY:APA91bFe4YSBx7bmcOrQSk3m0IwLFlE8FgwZc3CzMnecZ74XP7DIcOZzqXtQI7fPB3WHRax5CJ3qyanJg_ORlOF5wkkmijf-X4_qTfsjpHioeYVz2KDfZvAg6aNiUfTAOLHux-wN0ZDo';
            //header with content_type api key
            $headers = [
                'Content-Type:application/json',
                'Authorization:key=' . $server_key,
            ];
            //CURL request to route notification to FCM connection server (provided by Google)

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $notificationData);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            $result = curl_exec($ch);
            $requestData["result"] = $result;

            $notification->setUpdated(new \DateTime());

            if ($result === false) {
                $notification->setState(Notification::STATE_ERROR);
                $this->em->flush();
                die('Oops! FCM Send Error: ' . curl_error($ch));
            }

            $notification->setResult(is_bool($result) ? '' : $result);
            $notification->setState(Notification::STATE_OK);
            curl_close($ch);

            $this->em->flush();

            return $response->withJson($requestData, 200);
        }

        return $response->withStatus(400);
    }

}
