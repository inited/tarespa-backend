<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 14. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Contest;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class ContestRepository
 * @package App\Model\Repository
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestRepository extends BaseRepository implements ContestRepositoryInterface
{

    /**
     * {@inheritdoc}
     */
    public function findContest(int $id): Contest
    {
        try {
            $article = $this->_em->createQueryBuilder()
                ->select('contest')
                ->from(Contest::class, 'contest')
                ->andWhere('contest.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $article) {
                return $article;
            }

        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findContests(): array
    {
        return $this->_em->createQueryBuilder()
            ->select('contest.id', 'contest.order', 'contest.active', 'contest.contestUrl')
            ->addSelect('UNIX_TIMESTAMP(contest.timestamp) timestamp')
            ->from(Contest::class, 'contest')
            ->getQuery()
            ->getResult();
    }
}
