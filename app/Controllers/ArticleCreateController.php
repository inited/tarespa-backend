<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\Article;
use App\Model\ValueObject\ArticleValueObject;
use Doctrine\DBAL\Exception\ConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ArticleCreateController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArticleCreateController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ArticleCreateController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (is_array($data) && count($data)) {
            try {
                $entity = new Article();

                if (array_key_exists('key', $data)) {
                    $entity->setText((string)($data['key'] ?? ''));
                }

                if (array_key_exists('text', $data)) {
                    $entity->setKey((string)($data['text'] ?? ''));
                }

                $this->em->persist($entity);
                $this->em->flush();

                return $response->withJson(new ArticleValueObject($entity), 200);
            } catch (ConstraintViolationException $e) {
                $data = [
                    'error' => '"Key" is not unique!'
                ];

                return $response->withJson($data, 400);
            }

        }

        return $response->withStatus(400);
    }

}
