<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 13. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Contest
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Entity()
 * @ORM\Table(name="contest__contest")
 * @ORM\HasLifecycleCallbacks()
 */
final class Contest
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var bool
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = true;

    /**
     * @var string
     * @ORM\Column(name="contest_url")
     */
    private $contestUrl = '';

    /**
     * @var int
     * @ORM\Column(name="`order`", type="integer")
     */
    private $order = 1;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * Contest constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->timestamp = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     * @param PreUpdateEventArgs $event
     * @throws \Exception
     */
    public function checkEntityChangeSet(PreUpdateEventArgs $event): void
    {
        if ($event->hasChangedField('order') ||
            $event->hasChangedField('active') ||
            $event->hasChangedField('contestUrl')) {
            $this->timestamp = new \DateTime();
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getContestUrl(): string
    {
        return $this->contestUrl;
    }

    /**
     * @param string $contestUrl
     */
    public function setContestUrl(string $contestUrl): void
    {
        $this->contestUrl = $contestUrl;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void
    {
        $this->order = $order;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getTimestamp(): \DateTimeInterface
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTimeInterface $timestamp
     */
    public function setTimestamp(\DateTimeInterface $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

}
