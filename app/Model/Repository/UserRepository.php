<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InactiveUserException;
use App\Exceptions\InvalidCredentialsException;
use App\Exceptions\UserDeletedException;
use App\Model\Entity\UserEntity;
use App\Model\Entity\UserPasswordRequestEntity;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;

/**
 * Class UserRepository
 * @package App\Model\Repository
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
     * {@inheritdoc}
     */
    public function invalidUserPasswordChangeRequests(UserEntity $userEntity)
    {
        return $this->_em->createQueryBuilder()
            ->update(UserPasswordRequestEntity::class, 'u')
            ->set('u.valid', 0)
            ->where('u.user = :user')
            ->andWhere('u.valid = :valid')
            ->setParameters([
                'user' => $userEntity,
                'valid' => true,
            ])
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllUsers($onlyActive = false)
    {
        $query = $this->createQueryBuilder('u')
            ->select('u.id', 'u.firstName AS firstname', 'u.lastName AS lastname', 'c.name AS company')
            ->addSelect('u.email', 'u.active', 'UNIX_TIMESTAMP(u.lastLogin) AS lastlogin')
            ->addSelect('UNIX_TIMESTAMP(u.created) AS registered')
            ->join('u.company', 'c')
            ->where('u.deleted = :deleted')
            ->setParameters([
                'deleted' => false,
            ]);

        if ($onlyActive) {
            $query
                ->andWhere('u.active = :active')
                ->setParameter('active', $onlyActive);
        }

        return $query->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * {@inheritdoc}
     */
    public function verifyUser($email, $password)
    {

        /** @var UserEntity $user */
        $user = $this->findByEmail($email);


        if (!$user->isActive()) {
            throw new InactiveUserException();
        }

        if ($user->isDeleted()) {
            throw new UserDeletedException();
        }

        if (!password_verify($password, $user->getPassword())) {
            throw new InvalidCredentialsException();
        }

        return $user;

    }

    /**
     * {@inheritdoc}
     */
    public function findByEmail($email)
    {
        $user = $this->findOneBy([
            'email' => $email,
            'deleted' => false,
        ]);

        if ($user instanceof UserEntity) {
            return $user;
        }
        throw new EntityNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findValidPasswordCheckKey($key)
    {
        try {
            return $this->_em->createQueryBuilder()
                ->select('user_password_request')
                ->from(UserPasswordRequestEntity::class, 'user_password_request')
                ->where('user_password_request.hash = :key')
                ->andWhere('user_password_request.valid = :valid')
                ->andWhere('user_password_request.validTo >= :validTo')
                ->setParameters([
                    'key' => $key,
                    'valid' => true,
                    'validTo' => new \DateTime(),
                ])
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            throw new EntityNotFoundException();
        }
    }
}
