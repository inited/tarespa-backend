<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Device;
use App\Model\Repository\DeviceRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class NotificationsRegisterController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class NotificationsRegisterController extends DefaultController
{

    /**
     * @var DeviceRepositoryInterface
     */
    private $deviceRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * NotificationsRegisterController constructor.
     * @param DeviceRepositoryInterface $deviceRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(
        DeviceRepositoryInterface $deviceRepository,
        EntityManagerInterface $em
    )
    {
        $this->deviceRepository = $deviceRepository;
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (is_array($data) && count($data)) {
            if (array_key_exists('token', $data) && array_key_exists('platform', $data)) {
                if (empty($data['token'])) {
                    return $response->withStatus(400, 'Token is empty!');
                }

                if (!in_array($data['platform'], Device::getPlatforms())) {
                    return $response->withStatus(400, 'invalid platform!');
                }

                try {
                    $this->deviceRepository->findByToken($data['token']);
                    return $response->withStatus(400, 'Device is already registered!');
                } catch (EntityNotFoundException $e) {

                }

                $entity = new Device();
                $entity->setPlatform($data['platform']);
                $entity->setToken($data['token']);

                $this->em->persist($entity);
                $this->em->flush();

                return $response->withStatus(200);

            }
        }

        return $response->withStatus(400, 'Missing parameters!');
    }

}
