<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\ModelRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ModelDeleteController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ModelDeleteController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ModelRepositoryInterface
     */
    private $modelRepository;

    /**
     * ModelDeleteController constructor.
     * @param EntityManagerInterface $em
     * @param ModelRepositoryInterface $modelRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        ModelRepositoryInterface $modelRepository
    )
    {
        $this->em = $em;
        $this->modelRepository = $modelRepository;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $entity = $this->modelRepository->findModel(intval($args['id']));

                $this->em->remove($entity);
                $this->em->flush();

                return $response->withStatus(200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            }
        }

        return $response->withStatus(400);
    }

}
