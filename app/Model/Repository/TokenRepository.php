<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\InvalidAuthTokenException;
use App\Model\Entity\TokenEntity;
use App\Model\Entity\UserEntity;

/**
 * Class TokenRepository
 * @package App\Model\Repository\Token
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class TokenRepository extends BaseRepository implements TokenRepositoryInterface
{

    /**
     * @param UserEntity $entity
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function generateToken(UserEntity $entity)
    {
        $token = new TokenEntity();
        $token->setUser($entity);
        return $this->saveEntity($token);
    }

    /**
     * @param TokenEntity $token
     * @return bool
     */
    public function invalidateToken(TokenEntity $token)
    {
        try {
            $token->setValid(false);
            $this->saveEntity($token);
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * {@inheritdoc}
     */
    public function verifyToken($token)
    {
        /** @var TokenEntity $tokenEntity */
        $tokenEntity = $this->findOneBy([
            'id' => $token,
            'valid' => true,
        ]);

        if ($tokenEntity instanceof TokenEntity) {
            return $tokenEntity;
        }

        throw new InvalidAuthTokenException();
    }

    /**
     * @param UserEntity $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function invalidateUserTokens(UserEntity $user)
    {
        $tokens = $this->findBy([
            'user' => $user,
            'valid' => true,
        ]);
        if ($tokens) {
            /** @var TokenEntity $token */
            foreach ($tokens as $token) {
                $token->setValid(false);
            }
            $this->_em->flush();
        }
    }
}
